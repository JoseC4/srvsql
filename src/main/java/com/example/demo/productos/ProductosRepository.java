package com.example.demo.productos;

import com.example.demo.facturas.FacturaModel;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProductosRepository extends CrudRepository<ProductoModel, Integer> {

    Optional<ProductoModel> findById(int id);
}
