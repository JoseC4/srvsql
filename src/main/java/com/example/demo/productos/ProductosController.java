package com.example.demo.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/productosv2")
public class ProductosController {

    @Autowired
    ProductosService productosService;

    @PostMapping("/facturas")
    public String crearFactura(@RequestParam String cliente, @RequestParam int idProducto){
        try{
            productosService.crearFactura(cliente, idProducto);
            return "ok";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }
}
