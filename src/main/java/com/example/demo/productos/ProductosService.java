package com.example.demo.productos;

import com.example.demo.facturas.FacturaModel;
import com.example.demo.facturas.FacturasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;


@Service
public class ProductosService {


    @Autowired
    ProductosRepository productosRepository;

    @Autowired
    FacturasRepository facturasRepository;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void  crearFactura(String cliente, int idProducto) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = sdf.format(new Date());
        Optional<ProductoModel> optProd = productosRepository.findById(idProducto);
        if(optProd.isPresent()){
            ProductoModel productoModel =optProd.get();
            FacturaModel fm = new FacturaModel();
            fm.setCliente(cliente);
            fm.setId(idProducto);
            fm.setFecha(fecha);
            fm.setImporte(productoModel.getPrecio());
            facturasRepository.save(fm);
            if((productoModel.getStock() -1 <0)) throw new Exception("stock <0");
            productoModel.setStock(productoModel.getStock()-1);
            productosRepository.save(productoModel);
        }
    }


}
