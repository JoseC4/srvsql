package com.example.demo.facturas;

import com.example.demo.productos.ProductosService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apitechu/facturasv2")
public class FacturasController {
    @Autowired FacturasRepository facturasRepository;

    @GetMapping("/facturas")
    public Iterable<FacturaModel> getFacturas(@RequestParam(defaultValue = "0", name = "hasta") double hasta) {
        if(hasta==0){
         return facturasRepository.findAll();
        }
        return facturasRepository.findByImporte(hasta);
    }

    //No es correcta esta implementacion pero cuestiones del curso se hace asi
    @GetMapping("facturas/fecha")
    public Iterable<FacturaModel> getFacturaFecha(@RequestParam(defaultValue = "", name = "fecha") String fecha){
        if(fecha.isBlank()){
            return facturasRepository.findAll();
        }
        return facturasRepository.findByFecha(fecha);
    }

    @Autowired
    ProductosService productosService;

    @PostMapping("/facturas")
    public String crearFactura(@RequestParam String cliente, @RequestParam int idProducto){
            try{
                productosService.crearFactura(cliente, idProducto);
                return "ok";
            }catch (Exception ex){
                return ex.getMessage();
            }
        }

}
