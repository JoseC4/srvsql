package com.example.demo.facturas;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FacturasRepository extends CrudRepository<FacturaModel, Integer> {

    List<FacturaModel> findByImporte(double hasta);
    List<FacturaModel> findByFecha(String hasta);

}
